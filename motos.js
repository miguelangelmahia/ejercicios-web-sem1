
var motos = 
[
    {
      model: "YAMAHA X-MAX 250",
      preu: "1.300 €",
      kilometres: "68.000 km",
      cilindrada: "249 cc",
      lloc: "Barcelona",
      any: 2007
    },
    {
      model: "SUZUKI BURGMAN 200",
      preu: "1.200 €",
      kilometres: "45.000 km",
      cilindrada: "200 cc",
      lloc: "Barcelona",
      any: 2007
    },
    {
      model: "HONDA FORZA 250 X",
      preu: "1.599 €",
      kilometres: "15.000 km",
      cilindrada: "250 cc",
      lloc: "Barcelona",
      any: 2007
    },
    {
      model: "HONDA SCOOPY SH150i",
      preu: "1.600 €",
      kilometres: "49.000 km",
      cilindrada: "152 cc",
      lloc: "Barcelona",
      any: 2006
    },
    {
      model: "HONDA SCOOPY SH150i",
      preu: "1.400 €",
      kilometres: "54.000 km",
      cilindrada: "152 cc",
      lloc: "Barcelona",
      any: 2006
    },
    {
      model: "HONDA SCOOPY SH150i",
      preu: "1.000 €",
      kilometres: "40.200 km",
      cilindrada: "152 cc",
      lloc: "Barcelona",
      any: 2004
    },
    {
      model: "HONDA FORZA 250 X",
      preu: "1.200 €",
      kilometres: "69.000 km",
      cilindrada: "249 cc",
      lloc: "Barcelona",
      any: 2006
    },
    {
      model: "KYMCO Super Dink 300i",
      preu: "1.500 €",
      kilometres: "64.500 km",
      cilindrada: "300 cc",
      lloc: "Barcelona",
      any: 2010
    },
    {
      model: "PIAGGIO X7 250",
      preu: "1.600 €",
      kilometres: "20.000 km",
      cilindrada: "250 cc",
      lloc: "Barcelona",
      any: 2008
    },
    {
      model: "KYMCO Xciting 500",
      preu: "1.900 €",
      kilometres: "30.000 km",
      cilindrada: "498 cc",
      lloc: "Barcelona",
      any: 2007
    },
    {
      model: "YAMAHA X MAX 250",
      preu: "1.550 €",
      kilometres: "66.700 km",
      cilindrada: "250 cc",
      lloc: "Barcelona",
      any: 2005
    },
    {
      model: "HONDA SILVER WING 400",
      preu: "1.800 €",
      kilometres: "130.000 km",
      cilindrada: "398 cc",
      lloc: "Barcelona",
      any: 2008
    },
    {
      model: "YAMAHA Majesty 400",
      preu: "1.950 €",
      kilometres: "80.000 km",
      cilindrada: "395 cc",
      lloc: "Barcelona",
      any: 2004
    },
    {
      model: "SUZUKI BURGMAN 650",
      preu: "1.600 €",
      kilometres: "73.000 km",
      cilindrada: "650 cc",
      lloc: "Barcelona",
      any: 2002
    },
    {
      model: "YAMAHA Majesty 400",
      preu: "1.890 €",
      kilometres: "82.000 km",
      cilindrada: "395 cc",
      lloc: "Barcelona",
      any: 2005
    }
  ];